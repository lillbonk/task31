import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent  implements OnInit {


user = {
  username: '',
  password: ''
};

constructor(private authService: AuthService, private router: Router) { }

ngOnInit(): void {
}


 async onLoginClick(){


      const success: any = await this.authService.login(this.user);
      console.log(success);
      

      if (success) {
        this.router.navigateByUrl('/dashboard');
      } else {
        alert('NEIN, invlid username or password!')
      }
      
      
  }

}

