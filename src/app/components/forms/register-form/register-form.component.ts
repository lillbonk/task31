import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import { UserService } from 'src/app/services/auth/auth.service';


@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit{

  user = {
    username: '',
    password: '',
    confirmPassword: ''
  };

  constructor(private userService: UserService , private router: Router){}

  ngOnInit(): void {
  }  

  onRegisterClick() {


  try {

    if (this.user.password !== this.user.confirmPassword){
      return alert('Password do not match')
    }
    // exec the register fucntion from the service
    console.log(this.user);

    const result: any = this.userService.register(this.user);

    console.log(result);
    if (result){
      this.router.navigateByUrl('/dashboard');
    }
    

    
    
  } catch (e) {
    console.error(e);
    
    
  }
    
  }



}
